@extends('layouts.master')

@section('scripts')
    <script src="{{ asset('js/users.index.js') }}"></script>
@endsection

@section('content')

    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif

    <a href="{!! route('users.create') !!}" class="btn btn-info">Create</a>

    <table {!! (count($users)==0) ? 'style="display:none"' : '' !!} class="table table-bordered" id="userList">
        <thead>
            <th>Id</th>
            <th>Name</th>
            <th>Surname</th>
            <th>Email</th>
            <th>Action</th>
        </thead>
        <tbody>
    @foreach($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->surname }}</td>
            <td>{{ $user->email }}</td>
            <td>
                <a href="{{ route('users.edit',['id'=>$user->id]) }}" class="btn btn-info">Edit</a>
                <a href="#" data-url="{{ route('users.destroy',['id'=>$user->id]) }}" data-token="{{ Session::token() }}" class="btn btn-danger userDelete">Delete</a>
            </td>
        </tr>
    @endforeach
        </tbody>
    </table>
<div class="alert alert-warning" {!! (count($users)>0) ? 'style="display:none"' : '' !!} id="userListWarning">No users in database</div>

@endsection