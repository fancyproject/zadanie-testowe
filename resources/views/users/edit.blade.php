@extends('layouts.master')

@section('content')

    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif

    @if($errors->has())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger">{{ $error }}</div>
        @endforeach
    @endif

    {!! Form::model($user,['method'=>'PATCH','route' => ['users.update',$user->id]]) !!}

    <div class="form-group">
        {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('surname', 'Surname:', ['class' => 'control-label']) !!}
        {!! Form::text('surname', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('password', 'Password:', ['class' => 'control-label']) !!}
        {!! Form::password('password', ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>

    Permissions:<br>
    @foreach ($permissions as $permission)
        {!! Form::checkbox('permissions[]', $permission->id, in_array($permission->id,$userPermissions)) !!}
        {{ $permission->name }}
        <br>
    @endforeach


    {!! Form::submit('Update user', ['class' => 'btn btn-primary']) !!}

    <a href="{!! route('users.index') !!}" class="btn btn-info">Back</a>

    {!! Form::close() !!}
@endsection