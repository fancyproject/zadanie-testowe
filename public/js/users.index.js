$(document).ready(function(){
    $('.userDelete').click(function(){
        if(confirm("Are you sure?")){
            var handle = $(this);
            var url = handle.attr('data-url');
            var token = handle.attr('data-token');

            $.ajax({
                type:'POST',
                url: url,
                data: {
                    '_token': token,
                },
                success: function(){
                    handle.closest('tr').remove();

                    //hide table if no content and show warning
                    if($("#userList tbody tr").length==0){
                        $("#userList").hide();
                        $("#userListWarning").show();
                    }
                },
                error: function(){
                    alert('Failed!');
                }
            });
        }
        return false;
    });
});