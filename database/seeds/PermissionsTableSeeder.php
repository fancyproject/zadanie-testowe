<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'Remove', 'Create', 'Edit', 'Browse'
        ];

        foreach ($permissions as $name) {

            DB::table('permissions')->insert([
                'name' => $name
            ]);
        }
    }
}
