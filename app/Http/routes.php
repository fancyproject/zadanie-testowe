<?php

Route::resource('users', 'UsersController', ['except' => ['show','destroy']]);
Route::post('users/{id}/destroy',['as' => 'users.destroy', 'uses' => 'UsersController@destroy']);

Route::get('api/{id}','UsersApiController@get');
