<?php
/**
 * Created by PhpStorm.
 * User: Kuba
 * Date: 2015-08-04
 * Time: 12:56
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Response;

class UsersApiController extends Controller
{
    public function get($id)
    {
        $data = [];
        try {
            $data = User::with('permissions')->findOrFail($id);
            $code = 200;
        } catch (\Exception $e) {
            $code = 400;
            $data['error'] = $e->getMessage();
        }

        return Response::json($data, $code);
    }
}