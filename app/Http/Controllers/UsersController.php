<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Permission;
use App\User;
use Validator;
use Input;
use Redirect;
use Session;
use Response;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::all();

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $permissions = Permission::all();

        return view('users.add', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:2',
            'surname' => 'required|min:2',
            'password' => 'required|min:6',
            'email' => 'required|email|unique:users,email'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput($request->all());
        }

        $user = User::create($request->all());

        if($request->has('permissions')) {
            $user->permissions()->attach($request->get('permissions'));
        }

        Session::flash('flash_message', 'Success!');

        return Redirect::route('users.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $permissions = Permission::all();
        $userPermissions = $user->permissions()->lists('permission_id')->toArray();

        return view('users.edit', compact('user', 'permissions', 'userPermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $rules = [
            'name' => 'required|min:2',
            'surname' => 'required|min:2',
            'password' => 'required|min:6',
            'email' => 'required|email|unique:users,email,' . $id
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $user->fill($request->all())->save();

        if($request->has('permissions')) {
            $user->permissions()->sync($request->get('permissions'));
        }else{
            $user->permissions()->sync([]);
        }

        Session::flash('flash_message', 'Success!');

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);

            $user->permissions()->detach();

            $user->delete();

            $code = 200;
        } catch (\Exception $e) {
            $code = 400;
        }

        return Response::json([], $code);
    }
}
