<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class UserPermission extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_permissions';

    protected $fillable = ['user_id','permission_id'];
}
